﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Artemis;
using Artemis.Blackboard;
using Artemis.Manager;
using Artemis.System;
using Pocket_Engine.PocketComponents;
using Pocket_Engine.PocketSystems;

namespace Pocket_Engine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameEarlyTest : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        EntityWorld world;
        SysRenderer renderSystem;
        SysSpriteRenderer spriteSystem;
        SysAnimationRenderer animSystem;
        SysPlayerMovement playerSystem;

        Effect effect;
        RenderTarget2D rend;

        public GameEarlyTest()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            world = new EntityWorld();
            renderSystem = new SysRenderer();
            spriteSystem = new SysSpriteRenderer();
            animSystem = new SysAnimationRenderer();
            playerSystem = new SysPlayerMovement();

            world.SystemManager.SetSystem<SysRenderer>(renderSystem, GameLoopType.Draw);
            world.SystemManager.SetSystem<SysSpriteRenderer>(spriteSystem, GameLoopType.Draw);
            world.SystemManager.SetSystem<SysAnimationRenderer>(animSystem, GameLoopType.Draw);
            world.SystemManager.SetSystem<SysPlayerMovement>(playerSystem, GameLoopType.Update);

            Entity player = world.CreateEntity(0);
            CompTransform pos = new CompTransform(new Vector3(5, 5, 0));
            CompSprite sprite = new CompSprite(Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(System.IO.Directory.GetCurrentDirectory() + @"\Content\Sprites\" + "Player.png", System.IO.FileMode.Open)), new Rectangle(16,0,16,32));
            CompInput input = new CompInput();
            CompAnimation anim = new CompAnimation(pos.Position3D, CompAnimation.AnimationType.Sprite);
            player.AddComponent<CompTransform>(pos);
            player.AddComponent<CompSprite>(sprite);
            player.AddComponent<CompInput>(input);
            player.AddComponent<CompAnimation>(anim);

            Entity[,] frontBlocks = new Entity[3, 3];
            CompTransform fPos;
            CompSprite bSprite = new CompSprite(Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(System.IO.Directory.GetCurrentDirectory() + @"\Content\Sprites\" + "Block.png", System.IO.FileMode.Open)), new Rectangle(16, 0, 16, 32));
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    frontBlocks[x, y] = world.CreateEntity();
                    fPos = new CompTransform(new Vector3(4 + x, 6, y));
                    frontBlocks[x, y].AddComponent<CompTransform>(fPos);
                    frontBlocks[x, y].AddComponent<CompSprite>(bSprite);
                }
            }
            
            for (int i = 0; i < 5; i++)
            {
                Entity block = world.CreateEntity();
                CompTransform bPos = new CompTransform(new Vector3(5, 4, i));
                block.AddComponent<CompTransform>(bPos);
                block.AddComponent<CompSprite>(bSprite);
            }

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            rend = new RenderTarget2D(GraphicsDevice, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            effect = Content.Load<Effect>("Sprite Size Test");
            effect.Parameters["SpriteSize"].SetValue(16);
            effect.Parameters["TextureWidth"].SetValue(graphics.PreferredBackBufferWidth);
            effect.Parameters["TextureHeight"].SetValue(graphics.PreferredBackBufferHeight);
            effect.Parameters["UseLines"].SetValue(true);

            EntitySystem.BlackBoard.SetEntry("Player", world.EntityManager.GetEntity(0));
            EntitySystem.BlackBoard.SetEntry("SpriteBatch", spriteBatch);
            EntitySystem.BlackBoard.SetEntry("Graphics", graphics);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            EntitySystem.BlackBoard.SetEntry("GameTime", gameTime);
            world.Update();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.SetRenderTarget(rend);
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(samplerState: SamplerState.PointClamp, sortMode: SpriteSortMode.FrontToBack);
            world.Draw();
            spriteBatch.End();

            /*GraphicsDevice.SetRenderTarget(null);
            spriteBatch.Begin(samplerState: SamplerState.PointClamp, effect: effect);
            spriteBatch.Draw(rend, new Vector2(0, 0), Color.White);
            spriteBatch.End();*/

            base.Draw(gameTime);
        }
    }
}
