﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Artemis.Interface;

namespace Pocket_Engine.PocketComponents
{
    public class CompInput : IComponent
    {
        //Variables
        public enum KeyInput { ButtonA, ButtonB, Select, Start, Right, Left, Up, Down, ButtonR, ButtonL };
        private Dictionary<KeyInput, Keys> acceptedKeys;
        private Keys lastKey;

        //Properties
        public Dictionary<KeyInput, Keys> GetControlScheme
        {
            get
            {
                return acceptedKeys;
            }
        }
        public KeyInput? GetKeyInput
        {
            get
            {
                foreach(KeyValuePair<KeyInput, Keys> k in acceptedKeys)
                {
                    bool keyPressed = (Keyboard.GetState().IsKeyUp(lastKey) && Keyboard.GetState().IsKeyDown(k.Value));
                    lastKey = k.Value;
                    if (keyPressed)
                    {
                        return k.Key;
                    }
                }
                return null;
            }
        }
        public Keys GetKey
        {
            get
            {
                foreach (KeyValuePair<KeyInput, Keys> k in acceptedKeys)
                {
                    bool keyPressed = (Keyboard.GetState().IsKeyUp(lastKey) && Keyboard.GetState().IsKeyDown(k.Value));
                    lastKey = k.Value;
                    if (keyPressed)
                    {
                        return k.Value;
                    }
                }
                return Keys.None;
            }
        }

        //Constructors
        public CompInput()
        {
            acceptedKeys = new Dictionary<KeyInput, Keys>();
            acceptedKeys.Add(KeyInput.ButtonA, Keys.L);
            acceptedKeys.Add(KeyInput.ButtonB, Keys.K);

            acceptedKeys.Add(KeyInput.Select, Keys.R);
            acceptedKeys.Add(KeyInput.Start, Keys.Y);

            acceptedKeys.Add(KeyInput.Right, Keys.D);
            acceptedKeys.Add(KeyInput.Left, Keys.A);
            acceptedKeys.Add(KeyInput.Up, Keys.W);
            acceptedKeys.Add(KeyInput.Down, Keys.S);

            acceptedKeys.Add(KeyInput.ButtonR, Keys.O);
            acceptedKeys.Add(KeyInput.ButtonL, Keys.Q);
        }
        public CompInput(Dictionary<KeyInput, Keys> keys)
        {
            acceptedKeys = keys;
        }

        //Methods
        public bool KeyPressed(Keys key)
        {
            if (acceptedKeys.ContainsValue(key))
            {
                bool keyPressed = (Keyboard.GetState().IsKeyUp(lastKey) && Keyboard.GetState().IsKeyDown(key));
                lastKey = key;
                return keyPressed;
            }
            else
            {
                return false;
            }
        }
        public bool KeyPressed(KeyInput key)
        {
            if (acceptedKeys.ContainsKey(key))
            {
                bool keyPressed = (Keyboard.GetState().IsKeyUp(lastKey) && Keyboard.GetState().IsKeyDown(acceptedKeys[key]));
                lastKey = acceptedKeys[key];
                return keyPressed;
            }
            else
            {
                return false;
            }
        }
    }
}
