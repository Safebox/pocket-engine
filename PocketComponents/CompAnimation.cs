﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Artemis.Interface;

namespace Pocket_Engine.PocketComponents
{
    public class CompAnimation : IComponent
    {
        //Variables
        public enum AnimationType { Sprite, Background };
        private AnimationType type;
        private Vector3 lastPos;
        private Vector3 nextPos;
        private float animTime = 1f;

        //Properties
        public AnimationType Type
        {
            get
            {
                return CompAnimation.AnimationType.Sprite;
            }
            set
            {
                type = value;
            }
        }
        public Vector3 LastPosition
        {
            get
            {
                return lastPos;
            }
            set
            {
                lastPos = value;
            }
        }
        public Vector3 NextPosition
        {
            get
            {
                return nextPos;
            }
            set
            {
                nextPos = value;
            }
        }
        public bool IsMoving
        {
            get
            {
                return animTime < 1f;
            }
        }
        public float AnimationTime
        {
            get
            {
                return animTime;
            }
            set
            {
                animTime = (float)Math.Max(0, Math.Min(1, value));
            }
        }

        //Constructors
        public CompAnimation(Vector3 position, AnimationType animationType)
        {
            lastPos = position;
            nextPos = position;
            type = animationType;
        }
    }
}
