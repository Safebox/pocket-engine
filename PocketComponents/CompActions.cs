﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Artemis.Interface;

namespace Pocket_Engine.PocketComponents
{
    public class CompActions : IComponent
    {
        //Variables
        private List<EventActions.IAction> actionList;

        //Properties
        public List<EventActions.IAction> GetActions
        {
            get
            {
                return actionList;
            }
        }

        //Constructors
        public CompActions()
        {
        }
        public CompActions(List<EventActions.IAction> actions)
        {
            actionList = actions;
        }

        //Methods
        public void Update(GameTime gameTime)
        {
            foreach(EventActions.IAction action in actionList)
            {
                action.Update(gameTime);
            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (EventActions.IAction action in actionList)
            {
                action.Draw(gameTime, spriteBatch);
            }
        }
    }
}
