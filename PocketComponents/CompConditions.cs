﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Artemis.Interface;

namespace Pocket_Engine.PocketComponents
{
    public class CompConditions : IComponent
    {
        //Variables
        private Dictionary<string, bool> bools;
        private Dictionary<string, int> ints;

        //Properties
        public Dictionary<string, bool> BoolConditions
        {
            get
            {
                return bools;
            }
        }
        public Dictionary<string, int> IntConditions
        {
            get
            {
                return ints;
            }
        }

        //Constructors
        public CompConditions(Dictionary<string, bool> boolParameters)
        {
            bools = boolParameters;
        }
        public CompConditions(Dictionary<string, int> intParameters)
        {
            ints = intParameters;
        }

        //Methods
        public bool GetBoolByName(string name)
        {
            return bools[name];
        }
        public int GetIntByName(string name)
        {
            return ints[name];
        }
    }
}
