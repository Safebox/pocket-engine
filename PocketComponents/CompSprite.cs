﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Artemis.Interface;

namespace Pocket_Engine.PocketComponents
{
    public class CompSprite : IComponent
    {
        //Variables
        private Texture2D texture;
        private Rectangle coords;

        //Properties
        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }
        public Rectangle Frame
        {
            get
            {
                return coords;
            }
        }

        //Constructors
        public CompSprite(Texture2D texture, Rectangle coords)
        {
            this.texture = texture;
            this.coords = coords;
        }

        //Methods
        public void SetFrame(int index)
        {
            coords = new Rectangle(coords.X, (texture.Height / 4) * index, texture.Width / 3, texture.Height / 4);
        }
        public Rectangle RelativeFrame(float thisAngle, float angle)
        {
            int newRotation = (int)Math.Round(thisAngle + angle);
            int rotatedFrame = 0;
            switch (newRotation)
            {
                case 0:
                    rotatedFrame = 0;
                    break;
                case 90:
                    rotatedFrame = 1;
                    break;
                case 180:
                    rotatedFrame = 3;
                    break;
                case 270:
                    rotatedFrame = 2;
                    break;
                default:
                    rotatedFrame = 0;
                    break;
            }

            return new Rectangle(coords.X, coords.Y + (coords.Height * rotatedFrame), coords.Width, coords.Height);
        }
    }
}
