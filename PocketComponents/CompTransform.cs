﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Artemis.Interface;

namespace Pocket_Engine.PocketComponents
{
    public class CompTransform : IComponent
    {
        //Variables
        private Matrix transformMatrix;
        private float renderDepth;

        //Properties
        public Vector2 Position2D
        {
            get
            {
                Vector3 outPos = transformMatrix.Translation;
                return new Vector2(outPos.X, outPos.Y - outPos.Z);
            }
        }
        public Vector3 Position3D
        {
            get
            {
                return transformMatrix.Translation;
            }
            set
            {
                transformMatrix.Translation = value;
            }
        }
        public Quaternion Rotation
        {
            get
            {
                return transformMatrix.Rotation;
            }
            set
            {
                transformMatrix = transformMatrix * Matrix.CreateFromQuaternion(value);
            }
        }
        public float AngleRadians
        {
            get
            {
                return (float)Math.Atan2(2f * (transformMatrix.Rotation.W * transformMatrix.Rotation.Z + transformMatrix.Rotation.X * transformMatrix.Rotation.Y), 1f - 2f * (transformMatrix.Rotation.Y * transformMatrix.Rotation.Y + transformMatrix.Rotation.Z * transformMatrix.Rotation.Z));
            }
        }
        public float AngleDegrees
        {
            get
            {
                return (float)(AngleRadians / Math.PI * 180f);
            }
        }
        public float Scale
        {
            get
            {
                return (float)Math.Max(transformMatrix.Scale.X, Math.Max(transformMatrix.Scale.Y, transformMatrix.Scale.Z));
            }
            set
            {
                transformMatrix.Scale = new Vector3(value, value, value);
            }
        }
        public Vector3 Scale3D
        {
            get
            {
                return transformMatrix.Scale;
            }
            set
            {
                transformMatrix.Scale = value;
            }
        }
        public float RenderDepth
        {
            get
            {
                return renderDepth;
            }
        }
        
        //Constructors
        public CompTransform(Vector3 position)
        {
            transformMatrix = Matrix.CreateTranslation(position);
        }

        //Methods
        public Vector2 RelativePosition2D(Vector3 playerPos, Vector3 screenCenter, float radians)
        {
            Matrix rotatedMatrix = Matrix.CreateTranslation(transformMatrix.Translation - playerPos) * Matrix.CreateRotationZ(radians);
            Vector3 rotatedPos = screenCenter + rotatedMatrix.Translation;
            renderDepth = (rotatedMatrix.Translation.Y + rotatedMatrix.Translation.Z + 255f) / (255f * 2f);
            return new Vector2(rotatedPos.X, rotatedPos.Y - rotatedPos.Z);
        }
        public Vector3 RelativePosition3D(Vector3 playerPos, float radians)
        {
            Matrix rotatedMatrix = Matrix.CreateTranslation(transformMatrix.Translation - playerPos) * Matrix.CreateRotationZ(radians);
            Vector3 rotatedPos = playerPos + rotatedMatrix.Translation;
            return rotatedPos;
        }
    }

    public static class CompTransformExtensions
    {
        public static Vector2 Round(this Vector2 _this)
        {
            return new Vector2((int)Math.Round(_this.X), (int)Math.Round(_this.Y));
        }
        public static Vector3 Round(this Vector3 _this)
        {
            return new Vector3((int)Math.Round(_this.X), (int)Math.Round(_this.Y), (int)Math.Round(_this.Z));
        }
    }
}
