﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Artemis;
using Artemis.Attributes;
using Artemis.Manager;
using Artemis.System;

using Pocket_Engine.PocketComponents;

namespace Pocket_Engine.PocketSystems
{
    [ArtemisEntitySystem(ExecutionType = ExecutionType.Synchronous, GameLoopType = GameLoopType.Update)]
    public class SysPlayerMovement : EntityProcessingSystem
    {
        //Constructors
        public SysPlayerMovement() : base(Aspect.All(typeof(CompSprite), typeof(CompTransform), typeof(CompInput), typeof(CompAnimation)))
        {
        }

        //Methods
        public override void Process(Entity entity)
        {
            CompSprite spriteComponent = entity.GetComponent<CompSprite>();
            CompTransform transformComponent = entity.GetComponent<CompTransform>();
            CompInput inputComponent = entity.GetComponent<CompInput>();
            CompAnimation animComponent = entity.GetComponent<CompAnimation>();

            if (animComponent.IsMoving)
            {
                animComponent.AnimationTime += 1 / 15f;
                transformComponent.Position3D = Vector3.Lerp(animComponent.LastPosition, animComponent.NextPosition, animComponent.AnimationTime);
            }
            else
            {
                animComponent.LastPosition = animComponent.NextPosition;
                switch (inputComponent.GetKeyInput)
                {
                    case CompInput.KeyInput.Right:
                        Vector3 k = new Vector3((float)Math.Cos(transformComponent.AngleRadians), (float)Math.Sin(transformComponent.AngleRadians), 0);
                        animComponent.NextPosition = animComponent.LastPosition + new Vector3((float)Math.Cos(transformComponent.AngleRadians), (float)-Math.Sin(transformComponent.AngleRadians), 0);
                        break;
                    case CompInput.KeyInput.Left:
                        animComponent.NextPosition = animComponent.LastPosition - new Vector3((float)Math.Cos(transformComponent.AngleRadians), (float)-Math.Sin(transformComponent.AngleRadians), 0);
                        break;
                    case CompInput.KeyInput.Up:
                        animComponent.NextPosition = animComponent.LastPosition - new Vector3((float)Math.Sin(transformComponent.AngleRadians), (float)Math.Cos(transformComponent.AngleRadians), 0);
                        break;
                    case CompInput.KeyInput.Down:
                        animComponent.NextPosition = animComponent.LastPosition + new Vector3((float)Math.Sin(transformComponent.AngleRadians), (float)Math.Cos(transformComponent.AngleRadians), 0);
                        break;
                }
                animComponent.NextPosition = animComponent.NextPosition.Round();
                foreach (Entity o in EntityWorld.EntityManager.ActiveEntities)
                {
                    if (o.GetComponent<CompTransform>().Position3D == animComponent.NextPosition)
                    {
                        animComponent.NextPosition = animComponent.LastPosition;
                        break;
                    }
                }
                if (animComponent.NextPosition != animComponent.LastPosition)
                {
                    animComponent.AnimationTime = 0;
                }
            }
        }
    }
}
