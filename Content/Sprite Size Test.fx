﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D SpriteTexture;
int SpriteSize;
int TextureWidth;
int TextureHeight;
bool UseLines;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float2 pos = input.TextureCoordinates;
	float4 newCol;
	if (UseLines == true)
	{
		newCol = floor(min(fmod(pos.x * TextureWidth, SpriteSize), fmod(pos.y * TextureHeight, SpriteSize)));
		newCol = min(newCol, floor(min(fmod(pos.x * TextureWidth + 1, SpriteSize), fmod(pos.y * TextureHeight, SpriteSize))));
		newCol = min(newCol, floor(min(fmod(pos.x * TextureWidth, SpriteSize), fmod(pos.y * TextureHeight + 1, SpriteSize))));
		newCol = min(newCol, floor(min(fmod(pos.x * TextureWidth + 1, SpriteSize), fmod(pos.y * TextureHeight + 1, SpriteSize))));
	}
	else
	{
		newCol = floor((fmod(pos.x * TextureWidth, SpriteSize) + fmod(pos.y * TextureHeight, SpriteSize)) / 2);
		newCol = min(newCol, floor((fmod(pos.x * TextureWidth + 1, SpriteSize) + fmod(pos.y * TextureHeight, SpriteSize)) / 2));
		newCol = min(newCol, floor((fmod(pos.x * TextureWidth, SpriteSize) + fmod(pos.y * TextureHeight + 1, SpriteSize)) / 2));
		newCol = min(newCol, floor((fmod(pos.x * TextureWidth + 1, SpriteSize) + fmod(pos.y * TextureHeight + 1, SpriteSize)) / 2));
	}
	newCol.a = 1;
	return min(tex2D(SpriteTextureSampler,input.TextureCoordinates), newCol);
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};