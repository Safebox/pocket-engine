﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Pocket_Engine.EventActions.Message
{
    public class DisplayMessage : IAction
    {
        //Variables
        private string text;
        private Texture2D texture;
        private Rectangle position;
        private bool subtitleMode;

        //Properties
        public string Message
        {
            get
            {
                return text;
            }
        }
        public Texture2D MessageBox
        {
            get
            {
                return texture;
            }
        }
        public bool SubtitleMode
        {
            get
            {
                return subtitleMode;
            }
        }

        //Constructors
        public DisplayMessage(string text, Texture2D texture)
        {
            this.text = text;
            this.texture = texture;
        }
        
        //Methods
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (subtitleMode == false)
            {
                spriteBatch.Draw(texture, this.position, Color.White);
            }
            spriteBatch.Draw(texture, this.position, Color.White);
        }

        public void Update(GameTime gameTime)
        {
        }
    }
}
