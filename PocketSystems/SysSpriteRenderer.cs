﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Artemis;
using Artemis.Attributes;
using Artemis.Manager;
using Artemis.System;

using Pocket_Engine.PocketComponents;

namespace Pocket_Engine.PocketSystems
{
    [ArtemisEntitySystem(ExecutionType = ExecutionType.Synchronous, GameLoopType = GameLoopType.Draw)]
    public class SysSpriteRenderer : EntityProcessingSystem
    {
        //Constructors
        public SysSpriteRenderer() : base(Aspect.All(typeof(CompSprite), typeof(CompTransform), typeof(CompAnimation)))
        {
        }

        //Methods
        public override void Process(Entity entity)
        {
            CompSprite spriteComponent = entity.GetComponent<CompSprite>();
            CompTransform transformComponent = entity.GetComponent<CompTransform>();
            CompAnimation animationComponent = entity.GetComponent<CompAnimation>();
            CompTransform playerTransformComponent = EntityWorld.EntityManager.ActiveEntities[0].GetComponent<CompTransform>();
            
            if (animationComponent.Type == CompAnimation.AnimationType.Sprite)
            {
                ((SpriteBatch)BlackBoard.GetEntry("SpriteBatch")).Draw(spriteComponent.Texture, transformComponent.RelativePosition2D(((Entity)BlackBoard.GetEntry("Player")).GetComponent<CompTransform>().Position3D, new Vector3(((GraphicsDeviceManager)BlackBoard.GetEntry("Graphics")).PreferredBackBufferWidth / 2f - 8, ((GraphicsDeviceManager)BlackBoard.GetEntry("Graphics")).PreferredBackBufferHeight / 2f - 8, 0) / 16f, playerTransformComponent.AngleRadians) * 16f, new Rectangle(spriteComponent.Frame.X + (int)Math.Round(Math.Sin(animationComponent.AnimationTime * 2f * Math.PI)) * 16, spriteComponent.Frame.Y, spriteComponent.Frame.Width, spriteComponent.Frame.Height), Color.White, 0f, new Vector2(8, spriteComponent.Frame.Height - 8), transformComponent.Scale, SpriteEffects.None, transformComponent.RenderDepth);
            }
        }
    }
}
