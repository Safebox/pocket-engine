﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Artemis;
using Artemis.Attributes;
using Artemis.Manager;
using Artemis.System;

using Pocket_Engine.PocketComponents;

namespace Pocket_Engine.PocketSystems
{
    [ArtemisEntitySystem(ExecutionType = ExecutionType.Synchronous, GameLoopType = GameLoopType.Draw)]
    public class SysRenderer : EntityProcessingSystem
    {
        //Constructors
        public SysRenderer() : base(Aspect.All(typeof(CompSprite), typeof(CompTransform)))
        {
        }

        //Methods
        public override void Process(Entity entity)
        {
            if (entity.Id != 0)
            {
                CompSprite spriteComponent = entity.GetComponent<CompSprite>();
                CompTransform transformComponent = entity.GetComponent<CompTransform>();
                CompAnimation playerAnimComponent = EntityWorld.EntityManager.ActiveEntities[0].GetComponent<CompAnimation>();
                CompTransform playerTransformComponent = EntityWorld.EntityManager.ActiveEntities[0].GetComponent<CompTransform>();

                Vector3 rotCutaway = transformComponent.RelativePosition3D(playerTransformComponent.Position3D, playerTransformComponent.AngleRadians).Round();
                Vector3 playerPosAbs = playerTransformComponent.Position3D.Round();
                if (rotCutaway.Y - playerPosAbs.Y < -Math.Cos(playerTransformComponent.AngleRadians) ||
                    rotCutaway.Z - playerPosAbs.Z < Math.Sin(playerTransformComponent.AngleRadians))
                {
                    ((SpriteBatch)BlackBoard.GetEntry("SpriteBatch")).Draw(spriteComponent.Texture, transformComponent.RelativePosition2D(((Entity)BlackBoard.GetEntry("Player")).GetComponent<CompTransform>().Position3D, new Vector3(((GraphicsDeviceManager)BlackBoard.GetEntry("Graphics")).PreferredBackBufferWidth / 2f - 8, ((GraphicsDeviceManager)BlackBoard.GetEntry("Graphics")).PreferredBackBufferHeight / 2f - 8, 0) / 16f, playerTransformComponent.AngleRadians) * 16f, spriteComponent.RelativeFrame(transformComponent.AngleDegrees, playerTransformComponent.AngleDegrees), Color.White, 0f, new Vector2(8, spriteComponent.Frame.Height - 8), transformComponent.Scale, SpriteEffects.None, transformComponent.RenderDepth);
                }
                else if (rotCutaway.Z - playerPosAbs.Z < Math.Sin(playerTransformComponent.AngleRadians) + 2)
                {
                    ((SpriteBatch)BlackBoard.GetEntry("SpriteBatch")).Draw(spriteComponent.Texture, transformComponent.RelativePosition2D(((Entity)BlackBoard.GetEntry("Player")).GetComponent<CompTransform>().Position3D, new Vector3(((GraphicsDeviceManager)BlackBoard.GetEntry("Graphics")).PreferredBackBufferWidth / 2f - 8, ((GraphicsDeviceManager)BlackBoard.GetEntry("Graphics")).PreferredBackBufferHeight / 2f - 8, 0) / 16f, playerTransformComponent.AngleRadians) * 16f, spriteComponent.RelativeFrame(transformComponent.AngleDegrees, playerTransformComponent.AngleDegrees), new Color(1f, 1f, 1f, 0.1f), 0f, new Vector2(8, spriteComponent.Frame.Height - 8), transformComponent.Scale, SpriteEffects.None, transformComponent.RenderDepth);
                }
            }
        }
    }
}
