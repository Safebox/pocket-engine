﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Artemis;
using Artemis.Blackboard;
using Artemis.Manager;
using Artemis.System;
using Pocket_Engine.PocketComponents;
using Pocket_Engine.PocketSystems;

namespace Pocket_Engine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameRoomTest : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        EntityWorld world;
        SysRenderer renderSystem;
        SysSpriteRenderer spriteSystem;
        SysAnimationRenderer animSystem;
        SysPlayerMovement playerSystem;

        public GameRoomTest()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            world = new EntityWorld();
            renderSystem = new SysRenderer();
            spriteSystem = new SysSpriteRenderer();
            animSystem = new SysAnimationRenderer();
            playerSystem = new SysPlayerMovement();

            world.SystemManager.SetSystem<SysRenderer>(renderSystem, GameLoopType.Draw);
            world.SystemManager.SetSystem<SysSpriteRenderer>(spriteSystem, GameLoopType.Draw);
            world.SystemManager.SetSystem<SysAnimationRenderer>(animSystem, GameLoopType.Draw);
            world.SystemManager.SetSystem<SysPlayerMovement>(playerSystem, GameLoopType.Update);

            Entity player = world.CreateEntity(0);
            CompTransform pos = new CompTransform(new Vector3(5, 5, 0));
            pos.Rotation = Quaternion.CreateFromAxisAngle(new Vector3(0, 0, 1), 180f / 180f * 3.14f);
            CompSprite sprite = new CompSprite(Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(System.IO.Directory.GetCurrentDirectory() + @"\Content\Sprites\" + "Player.png", System.IO.FileMode.Open)), new Rectangle(16,0,16,32));
            CompInput input = new CompInput();
            CompAnimation anim = new CompAnimation(pos.Position3D, CompAnimation.AnimationType.Sprite);
            player.AddComponent<CompTransform>(pos);
            player.AddComponent<CompSprite>(sprite);
            player.AddComponent<CompInput>(input);
            player.AddComponent<CompAnimation>(anim);

            CompSprite bSprite = new CompSprite(Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(System.IO.Directory.GetCurrentDirectory() + @"\Content\Sprites\" + "Block.png", System.IO.FileMode.Open)), new Rectangle(16, 0, 16, 32));
            CompTransform bPos;
            for (int x = 0; x < 5; x++)
            {
                for (int y = 0; y < 5; y++)
                {
                    for (int z = 0; z < 4; z++)
                    {
                        if (z == 3 || (z < 3 && (x == 0 || x == 4 || y == 0 || y == 4) && (x != 2 || y != 4 || z >=2)))
                        {
                            Entity block = world.CreateEntity();
                            bPos = new CompTransform(new Vector3(3 + x, 3 + y, z));
                            block.AddComponent<CompTransform>(bPos);
                            block.AddComponent<CompSprite>(bSprite);
                        }
                    }
                }
            }

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            EntitySystem.BlackBoard.SetEntry("Player", world.EntityManager.GetEntity(0));
            EntitySystem.BlackBoard.SetEntry("SpriteBatch", spriteBatch);
            EntitySystem.BlackBoard.SetEntry("Graphics", graphics);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            EntitySystem.BlackBoard.SetEntry("GameTime", gameTime);
            world.Update();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.SetRenderTarget(rend);
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(samplerState: SamplerState.PointClamp, sortMode: SpriteSortMode.FrontToBack);
            world.Draw();
            spriteBatch.End();

            /*GraphicsDevice.SetRenderTarget(null);
            spriteBatch.Begin(samplerState: SamplerState.PointClamp, effect: effect);
            spriteBatch.Draw(rend, new Vector2(0, 0), Color.White);
            spriteBatch.End();*/

            base.Draw(gameTime);
        }
    }
}
